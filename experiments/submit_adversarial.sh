#!/bin/bash

# Slurm parameters
PART=candes       # Partition names
MEMO=1G           # Memory required
TIME=00-06:00:00  # Time required
CORE=1            # Cores required

# Assemble order prefix
ORDP="sbatch --mem="$MEMO" -n 1 -c "$CORE" -p "$PART" --time="$TIME

# Create directory for log files
LOGS=logs
mkdir -p $LOGS

# Parameters
N_LIST=(50 100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000)
#N_LIST=(1000)

for N in "${N_LIST[@]}"; do
  # Script to be run
  SCRIPT="adversarial-example.sh $N"
  # Define job name for this chromosome
  JOBN="adv_n"$N
  OUTF=$LOGS"/"$JOBN".out"
  ERRF=$LOGS"/"$JOBN".err"
  # Assemble slurm order for this job
  ORD=$ORDP" -J "$JOBN" -o "$OUTF" -e "$ERRF" "$SCRIPT
    # Print order
  echo $ORD
  # Submit order
  $ORD
done
