#!/bin/bash

mkdir -p /scratch/groups/candes/gene_hunting_discussion/
mkdir -p /scratch/groups/candes/gene_hunting_discussion/adversarial/
mkdir -p /scratch/groups/candes/gene_hunting_discussion/adversarial/results/

ml R/3.5.1
ml gcc

Rscript --vanilla adversarial-example.R $1
